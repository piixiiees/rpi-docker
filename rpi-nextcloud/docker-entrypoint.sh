#!/bin/bash

set -e

if [ -z "$NEXTCLOUD_SERVERNAME" ]; then
    echo >&2 'error: you have to provide a server-name'
    echo >&2 '  Did you forget to add -e HOSTNAME=... ?'
    exit 1
fi

sudo sed -i "s/server_name localhost/server_name $NEXTCLOUD_SERVERNAME/g" /etc/nginx/sites-available/default

if [ ! -f /etc/nginx/ssl/nextcloud.crt ]; then
    sudo mkdir /etc/nginx/ssl 
    sudo openssl genrsa -out /etc/nginx/ssl/nextcloud.key 4096 
    sudo openssl req -new -sha256 -batch -subj "/CN=$NEXTCLOUD_SERVERNAME" -key /etc/nginx/ssl/nextcloud.key -out /etc/nginx/ssl/nextcloud.csr 
    sudo openssl x509 -req -sha256 -days 3650 -in /etc/nginx/ssl/nextcloud.csr -signkey /etc/nginx/ssl/nextcloud.key -out /etc/nginx/ssl/nextcloud.crt
fi

exec "$@"
