FROM schoeffm/rpi-nginx-php5
MAINTAINER Stefan Schoeffmann <stefan.schoeffmann@posteo.de>

ENV NEXTCLOUD_VERSION nextcloud-9.0.53
ENV NEXTCLOUD_BASE_DIR /var/www
ENV NEXTCLOUD_DATA_DIR /srv/http/nextcloud/data
ENV NEXTCLOUD_CONFIG_DIR $NEXTCLOUD_BASE_DIR/nextcloud/config
ENV NEXTCLOUD_SERVERNAME dockerpi

RUN apt-get update && apt-get install -y wget bzip2 vim

# Change upload-limits and -sizes
RUN sudo sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 2048M/g" /etc/php5/fpm/php.ini && \
    sudo sed -i "s/post_max_size = 8M/post_max_size =root123  2048M/g" /etc/php5/fpm/php.ini && \
    sudo echo 'default_charset = "UTF-8"' >> /etc/php5/fpm/php.ini && \
    echo "upload_tmp_dir = $NEXTCLOUD_DATA_DIR" >> /etc/php5/fpm/php.ini && \
    echo "extension = apc.so" >> /etc/php5/fpm/php.ini && \
    echo "apc.enabled = 1" >> /etc/php5/fpm/php.ini && \
    echo "apc.include_once_override = 0" >> /etc/php5/fpm/php.ini && \
    echo "apc.shm_size = 256" >> /etc/php5/fpm/php.ini 

# now add our hand-written nginx-default-configuration which makes use of all the stuff so far prepared
ADD default /etc/nginx/sites-available/default

# Create the data-directory where NEXTCLOUD can store its stuff
RUN sudo mkdir -p "$NEXTCLOUD_DATA_DIR" && \
    sudo chown www-data:www-data "$NEXTCLOUD_DATA_DIR"

# finally, download NEXTCLOUD and extract it
RUN sudo mkdir /var/www 
WORKDIR /var/www

RUN sudo wget https://download.nextcloud.com/server/releases/"$NEXTCLOUD_VERSION".tar.bz2 && \
    sudo tar xvf "$NEXTCLOUD_VERSION".tar.bz2 && \
    sudo rm "$NEXTCLOUD_VERSION".tar.bz2 && \
    sudo chown -R www-data:www-data nextcloud

WORKDIR /
COPY docker-entrypoint.sh /entrypoint.sh

VOLUME "$NEXTCLOUD_DATA_DIR"
VOLUME "$NEXTCLOUD_CONFIG_DIR"

ENTRYPOINT ["/entrypoint.sh"]
CMD service php5-fpm start && nginx
