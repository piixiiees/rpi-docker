Based on this image you can create a ready-to-be-used nextcloud-installation on your RaspberryPi (backed by a MySQL-Database). 

Basically this image (and all dependent images) are an adjusted copy of the original owncloud-images (like `schoeffm/rpi-owncloud`, `schoeffm/rpi-owncloud-data` etc).

You can find the `Dockerfile`, all companion scripts as well as the `docker-compose.yml`-file in this [BitBucket-Repo][repo]

For a cut'n'paste able Screencast see the sections below.

## Basic Setup

In order to keep things separated and to support a smooth update-process for future nextcloud releases the whole installation consists of three different container.

	rpi-nextcloud  --+							// actual installation
	                 |
	                 +---> rpi-nextcloud-db		// mysql database-backend
	                 |
	                 +---> rpi-nextcloud-data	// volues for uploaded files and configs
	
This installation uses a mysql database as backend. The database is encapsulated in a dedicated container (`rpi-nextcloud-db`) which in turn is linked to the actual nextcloud-container (`rpi-nextcloud`). 

I also thought it would be benificial to separate the uploaded data and the configuration from the actual nextcloud installation to make upgrades much easier. That's the reason for the `rpi-nextcloud-data` container which just provides volumes to store these data.

## (Recommended) Usage

### Use docker-compose

[![asciicast](https://asciinema.org/a/80518.png)](https://asciinema.org/a/80518)

To get all three container up and running with one command you should use `docker-compose`. Thanks to the guys from hypriot we have now also the possibility to use [that tool][docker-compose] on our RaspberryPi. 

So, install it, check out the repo and run `sudo docker-compose up` - that's it. 

_Notice: don't forget to change the passwords in the YAML-file_

### Start everything by hand

So, how to get started. If you'd like to start everything manually you can do it this way:

[![asciicast](https://asciinema.org/a/80518.png)](https://asciinema.org/a/80518)

#### start a MySQL-Instance

    	sudo docker run -d -p 3306:3306 --name nextcloud-db \
    		-e NEXTCLOUD_DB_USER=nextcloud \
    		-e NEXTCLOUD_DB_PASSWORD=mycloud \
    		-e MYSQL_ROOT_PASSWORD=foo \
    		schoeffm/rpi-nextcloud-mysql

This will start a mysql deamonized container. In order to be useful for our purposes we have to provide a password for the root-user as well as a username and password for the technical nextcloud user. The running container provides a `nextcloud`-schema which is accessible by the given user. Next ...

#### create a data-volume

    	sudo docker run -d --name nextcloud-data \
            -e NEXTCLOUD_DB_USER=nextcloud \
            -e NEXTCLOUD_DB_PASSWORD=mycloud \
            schoeffm/rpi-nextcloud-data

After running this container it will immediately stop running since its only purpose is to provide a bunch of volumes for our actual nextcloud-container - and for that it mustn't be running. As mentioned before, this should make updates of your nextcloud-container much easier since you don't have to worry about your uploaded files or your `config.php`. Also notice, that you have to pass the very same DB-credentials to the data-container - since it hosts the config.php for your nextcloud and the connection is configured their. Finally ...

#### combine everything

    	sudo docker run -d --name nextcloud -p 80:80 -p 443:443 \
    		--link nextcloud-db:mysql --volumes-from nextcloud-data \ 
    		-e NEXTCLOUD_SERVERNAME=your.domain.org schoeffm/rpi-nextcloud

This is acutal nextcloud installation which is accessible via port 80 and 443 (whereas 80 just redirects to 443 - we won't accept unsecured communication).

The overall setup is based on the very good tutorials of [Jan Karres][owncloud]. Some of the more important things include:

- [nginx][nginx_page] as web-server (smaller footprint, good performance)
- only secured communication over port SSL (port 80 will be redirected to 443)
- thus, on first start a self-signed certificate will be created (be sure you provide a valid `NEXTCLOUD_SERVERNAME`)
- extended file-upload limits in your `php.ini` (2048M)
- preconfigured UTF-8 support
- ... _(see [here][nginx] and [here][owncloud] for more details)_

## How does an upgrade look like

Nextcloud provide different ways to upgrade an installation. Our approach is pretty much compareable with the [maunal upgrade][upgrade] - which you still should read through!

1. it is always wise to backup your data before you start to change your current setup
    - dump the database-content (i.e. by using the [mysql-workbench][mysql])
    - dump the data contained in our volume 

            sudo docker run --rm --volumes-from nextcloud-data \
                -v $(pwd):/backup resin/rpi-raspbian \
                tar cvf /backup/nextcloud-data-backup.tar /srv

2. now, turn on maintenance mode
    
        sudo docker exec -u www-data -d nextcloud \
            php /var/www/nextcloud/occ maintenance:mode --on

2. stop the currently running instance: 
        
        sudo docker stop nextcloud
3. start the new version by creating a new container (_as we've described above - don't forget to give it a new name or remove the old one first_)
4. now, execute the upgrade script (_within the new, running container_)

        sudo docker exec -u www-data -d nextcloud \
            php /var/www/nextcloud/occ upgrade

5. finally, turn off the maintenance mode again
    
        sudo docker exec -u www-data -d nextcloud \
            php /var/www/nextcloud/occ maintenance:mode --off

That's it!

[repo]:https://bitbucket.org/schoeffm/rpi-docker/src/acb764e933b38ff486f62417717cdf9d1a18d7fc/rpi-nextcloud/?at=master
[docker-compose]:http://blog.hypriot.com/post/docker-compose-nodejs-haproxy/
[nginx]:http://jankarres.de/2012/08/raspberry-pi-webserver-nginx-installieren/
[owncloud]:http://jankarres.de/2013/10/raspberry-pi-owncloud-server-installieren/
[nginx_page]:http://nginx.com
[upgrade]:https://docs.nextcloud.org/server/9/admin_manual/maintenance/upgrade.html?highlight=upgrade
[mysql]:https://www.mysql.de/products/workbench/
